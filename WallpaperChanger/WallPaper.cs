﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;

namespace WallpaperChanger
{
	/// <summary>
	/// Represents an object that provides access to Windows 7 wallpapers.
	/// </summary>
	public sealed class WallPaper
	{
		private const int SPI_SETDESKWALLPAPER = 20;
		private const int SPIF_UPDATEINIFILE = 0x01;
		private const int SPIF_SENDWININICHANGE = 0x02;
		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		private static extern int SystemParametersInfo(int uAction, int uParam, string lpvParam, int fuWinIni);
		/// <summary>
		/// Enumeration of wallpaper display styles.
		/// </summary>
		public enum Style
		{
			/// <summary>
			/// The wallpaper will be tiled around the screen.
			/// </summary>
			Tiled = 16,
			/// <summary>
			/// The wallpaper will be centered on the screen.
			/// </summary>
			Centered = 21,
			/// <summary>
			/// The wallpaper will be stretched to fit the screen resolution.
			/// </summary>
			Stretched = 12
		}
		/// <summary>
		/// Sets a desktop wallpaper.
		/// </summary>
		/// <param name="file"> Path to the image file to become a wallpaper.</param>
		/// <param name="style">Style to use for the wallpaper.</param>
		public static void Set(string file, Style style)
		{
			// Check if the image can be loaded.
			try
			{
				using (Image image = Image.FromFile(file))
				{
				}
			}
			catch (OutOfMemoryException)
			{
				MessageBox.Show("Unsupported image format.");
			}
			catch (FileNotFoundException)
			{
				MessageBox.Show("File not found.");
			}
			// Set the wallpaper style through registry.
			RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Control Panel\Desktop", true);
			if (key != null)
			{
				CultureInfo info = CultureInfo.InvariantCulture;
				key.SetValue(@"WallpaperStyle", ((int)style % 5).ToString(info));
				key.SetValue(@"TileWallpaper", ((int)style % 3).ToString(info));
			}
			// Tell Windows to set a new wallpaper.
			SystemParametersInfo
				(
					SPI_SETDESKWALLPAPER,
					0,
					file,
					SPIF_UPDATEINIFILE | SPIF_SENDWININICHANGE
				);
		}
	}
}